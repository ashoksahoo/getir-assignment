# getir assignment
Sample API application

## Requirements

* Node.js >= 14
* MongoDb >= 4.2


## Usage

Heroku EndPoint: https://getir-assignament.herokuapp.com/api/v1/records

```
curl --location --request POST 'https://getir-assignament.herokuapp.com/api/v1/records' \
--header 'Content-Type: application/json' \
--data-raw '{
    "startDate": "2016-01-26",
    "endDate": "2018-02-02",
    "minCount": 2700,
    "maxCount": 3000
}'
```



### Dev setup

To install Dependencies 
`npm install`

To Run Tests
`npm test`

Run Application:
`npm start`

Run Application in prod:

```
NODE_ENV="production" MONGODB_URI="mongodb://localhost/getir-case-study" npm start
```

### Request samples

Request records using curl:

```
curl --location --request POST 'https://getir-assignament.herokuapp.com/api/v1/records' \
--header 'Content-Type: application/json' \
--data-raw '{
    "startDate": "2016-01-26",
    "endDate": "2018-02-02",
    "minCount": 2700,
    "maxCount": 3000
}'
```


## Config


* **PORT**: Listening Port, default Value 3000.
* **MONGODB_URI**: string -- mongodb URI,
* **LOG_LEVEL**: string -- log level, 


## Service API

This is an HTTP API that uses JSON format. Thus, if an endpoint consumes JSON
payload (see endpoints below) then `content-type: application/json`
request header should be provided.

### POST /api/v1/records

The request payload will include a JSON with 4 fields.

```json
{
  "startDate": "2016-01-26",
  "endDate": "2018-02-02",
  "minCount": 2700,
  "maxCount": 3000
}
```

Response payload (JSON):

* **code** - HTTP Status code
    * 0 - Success
    * 400 - Bad request,
    * 404 - Resource is not found
    * 500 - Internal server error
* **msg** - message for the status code
* **records** - results as per request

Sample:

```json
{
  "code": 0,
  "msg": "Success",
  "records": [
    {
      "key": "pxClAvll",
      "createdAt": "2016-12-19T10:00:40.050Z",
      "totalCount": 2772
    },
    {
      "key": "KYKAKxDr",
      "createdAt": "2016-11-27T00:30:34.725Z",
      "totalCount": 2713
    }
  ]
}
```
