const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./utils/logger');
const {MongoClient} = require('mongodb');

const registerRoutes = require('./routes');
const config = require('./utils/config');
const {errorMiddleware,notFoundErrorMiddleware} = require('./utils/middlewares');
const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(bodyParser.json());


const mongoClient = new MongoClient(config.mongodbUri, {
    useUnifiedTopology: true
});
mongoClient.connect().then(() => {
    logger.info('Connection to mongo opened');
    app.set('db', mongoClient.db());
    registerRoutes(app)
    app.use(notFoundErrorMiddleware);
    app.use(errorMiddleware);



}).catch((err) => {
    logger.error('Error in setting up server:', err);
    process.exit(-1);
});


module.exports = app;
