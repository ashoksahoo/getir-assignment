const express = require("express");
const serverRoutes = require("../routes/index");
const bodyParser = require('body-parser');
const request = require("supertest");
const assert = require("assert");
const {MongoClient} = require('mongodb');
const {errorMiddleware, notFoundErrorMiddleware} = require('../utils/middlewares');


const app = express();
app.use(bodyParser.json());
serverRoutes(app) //routes

app.use(notFoundErrorMiddleware);
app.use(errorMiddleware);
let mongoClient;

describe("testing-server-routes", () => {
    afterAll(done => {
        mongoClient.close();
        done();
    });
    beforeAll(async () => {
        mongoClient = new MongoClient("mongodb://localhost:27017", {
            useUnifiedTopology: true
        });
        await mongoClient.connect()
        app.set('db', mongoClient.db());
    })
    it("POST /api/v1/records - success", async () => {
        const payload = {
            "startDate": "2016-01-26",
            "endDate": "2016-02-02",
            "minCount": 1,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(200);
        assert.ok(response.text.includes("Success"));
    });
    it("POST /api/v2/records - 404 Error", async () => {
        const payload = {
            "startDate": "2016-01-26",
            "endDate": "2016-02-02",
            "minCount": 1,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v2/records").send(payload).expect(404)
        assert.ok(response.text.includes("Not Found"));
    });

    it("POST /api/v1/records - Bad Input", async () => {
        const payload = {
            "startDate": "2016-01-261",
            "endDate": "2016-02-02",
            "minCount": 1,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(400)
        assert.ok(response.text.includes("startDate must be a valid date"));
    });

    it("POST /api/v1/records - Bad Input", async () => {
        const payload = {
            "startDate": "2016-01-26",
            "endDate": "2016-02-024",
            "minCount": 1,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(400)
        assert.ok(response.text.includes("endDate must be a valid date"));
    });

    it("POST /api/v1/records - Bad Input", async () => {
        const payload = {
            "startDate": "2016-01-26",
            "endDate": "2016-02-02",
            "minCount": 200,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(400)
        assert.ok(response.text.includes("minCount must be less or equal maxCount"));
    });

    it("POST /api/v1/records - Bad Input", async () => {
        const payload = {
            "endDate": "2016-01-26",
            "startDate": "2016-02-02",
            "minCount": 200,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(400)
        assert.ok(response.text.includes("startDate must be less than endDate"));
    });

    it("POST /api/v1/records - Bad Input", async () => {
        const payload = {
            "endDate": "2016-01-26",
            "startDate": "2016-02-02",
            "minCount": 200,
            "maxCount": 100
        }
        let response = await request(app).post("/api/v1/records").send(payload).expect(400)
        assert.ok(response.text.includes("startDate must be less than endDate"));
    });
});
