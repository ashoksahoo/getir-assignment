const recordRouter = require('./records');

function registerRoutes(app){
    app.use('/api/v1/records', recordRouter);
}
module.exports = registerRoutes;
