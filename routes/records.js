const express = require('express');
const logger = require('../utils/logger');
const router = express.Router();
const {body, validationResult} = require('express-validator');
const {DateTime} = require('luxon');
const createError = require('http-errors')
const {InternalServerError, BadRequestError} = require('../utils/errors')

router.post('/',
    body('startDate').notEmpty().withMessage("startDate must be a valid date"),
    body('endDate').notEmpty().withMessage("endDate must be a valid date"),
    body('minCount').notEmpty().isInt({min: 0}).withMessage("minCount must be a valid integer"),
    body('maxCount').notEmpty().isInt({min: 1}).withMessage("maxCount must be a valid integer & grater than minCount"),
    async function (req, res, next) {
        const errors = validationResult(req);
        let {minCount, maxCount, startDate, endDate} = req.body;

        startDate = DateTime.fromISO(startDate, {zone: 'GMT'});
        if (!startDate.isValid) {
            return next(new BadRequestError('startDate must be a valid date'));
        }
        endDate = DateTime.fromISO(endDate, {zone: 'GMT'});
        if (!endDate.isValid) {
            return next(new BadRequestError('endDate must be a valid date'));
        }
        if (startDate.toMillis() >= endDate.toMillis()) {
            return next(new BadRequestError('startDate must be less than endDate'));
        }
        if (minCount > maxCount) {
            return next(new BadRequestError('minCount must be less or equal maxCount'));
        }

        if (!errors.isEmpty()) {
            return next(new BadRequestError(errors.array().map(e => e.msg).join(" ")));
        }


        const pipeline = [{
            $match: {createdAt: {$gte: startDate.toJSDate(), $lt: endDate.toJSDate()}}
        }, {
            $addFields: {totalCount: {$sum: '$counts'}}
        }, {
            $match: {totalCount: {$gte: minCount, $lte: maxCount}}
        }, {
            $project: {
                _id: 0, key: 1, createdAt: 1, totalCount: 1
            }
        }];

        const db = req.app.get('db');
        try {
            const coll = db.collection('records');
            const records = await coll.aggregate(pipeline).toArray();
            res.send({code: 0, msg: 'Success', records});
        } catch (e) {
            return next(new InternalServerError(e.msg));
        }


    });

module.exports = router;
