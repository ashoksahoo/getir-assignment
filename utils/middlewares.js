const {NotFoundError} = require('./errors')

function errorMiddleware(err, req, res, next) {
    res.status(err.code || 500)
    res.json({
        code: err.code || 500,
        message: err.message || 'Internal server error'
    });
}

function notFoundErrorMiddleware(req, res, next) {
    return next(new NotFoundError("Resource Not Found, Here is a cat Instead ^| o_o |^"));

}

module.exports = {
    errorMiddleware, notFoundErrorMiddleware
}
