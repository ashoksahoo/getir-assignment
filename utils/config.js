function getConfig() {

    let devConfig = {
        port: 3000,
        mongodbUri: "mongodb://localhost/getir-case-study",
        // mongodbUri: "mongodb://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true",
        logLevel:  'info'
    };

    let prodConfig = {
        port: process.env.PORT || 3000,
        mongodbUri: process.env.MONGODB_URI,
        logLevel: 'error'
    };

    if (process.env.NODE_ENV === "production") {
        return prodConfig
    } else {
        return devConfig
    }
};
module.exports = getConfig();
