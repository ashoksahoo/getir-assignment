const winston = require('winston');
const config = require('./config');

const logger = winston.createLogger({
    level: config.logLevel,
    format: winston.format.json(),
    defaultMeta: { service: 'getir-app' },
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        new winston.transports.Console({
            format: winston.format.simple(),
        })
    ]
});

module.exports = logger
