class ValidationError extends Error {
    constructor(message) {
        super(message); // (1)
        this.name = "ValidationError"; // (2)
        this.message = message;
        this.code = 400;
    }
}
class BadRequestError extends Error {
    constructor(message) {
        super(message);
        this.name = "BadRequestError";
        this.message = message;
        this.code = 400;
    }
}

class InternalServerError extends Error {
    constructor(message) {
        super(message);
        this.name = "InternalServerError";
        this.message = message;
        this.code = 500;
    }
}

class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.name = "NotFoundError";
        this.message = message;
        this.code = 404;
    }
}


module.exports = {
    BadRequestError,InternalServerError,ValidationError,NotFoundError
}
